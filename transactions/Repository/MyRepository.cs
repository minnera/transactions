﻿using Repository.AccountsRepos;
using Repository.AddressesRepos;
using Repository.CardAccountsConnectionsRepos;
using Repository.CardsRepos;
using Repository.PeopleRepos;
using Repository.TraderAddressConnectionsRepos;
using Repository.TraderPersonConnectionsRepos;
using Repository.TradersRepos;
using Repository.TransactionsRepos;

namespace Repository
{
    public class MyRepository
    {
        public IAccountsRepository accountsRepo { get; private set; }
        public IAddressesRepository addressesRepo { get; private set; }
        public ICardAccountsConnectionsRepository cardAccountsConnectionsRepo { get; private set; }
        public ICardsRepository cardsRepo { get; private set; }
        public IPeopleRepository peopleRepo { get; private set; }
        public ITraderAddressConnectionsRepository traderAddressConnectionsRepo { get; private set; }
        public ITraderPersonConnectionsRepository traderPersonConnectionsRepo { get; private set; }
        public ITradersRepository tradersRepo { get; private set; }
        public ITransactionsRepository transactionsRepo { get; private set; }

        public MyRepository(IAccountsRepository acr, IAddressesRepository adr, ICardAccountsConnectionsRepository cacr,
            ICardsRepository cr, IPeopleRepository pr, ITraderAddressConnectionsRepository tacr,
            ITraderPersonConnectionsRepository tpcr, ITradersRepository tdr, ITransactionsRepository tcr)
        {
            accountsRepo = acr;
            addressesRepo = adr;
            cardAccountsConnectionsRepo = cacr;
            cardsRepo = cr;
            peopleRepo = pr;
            traderAddressConnectionsRepo = tacr;
            traderPersonConnectionsRepo = tpcr;
            tradersRepo = tdr;
            transactionsRepo = tcr;
        }
    }
}