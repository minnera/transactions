﻿using System;
using Data;
using Repository.GenericRepos;

namespace Repository.CardsRepos
{
    public interface ICardsRepository : IRepository<CARDS>
    {
        //datetime.date
        void Modify(int id, string cardnumber, DateTime expiration, string status, string type, bool additional,
            int owner);
    }
}