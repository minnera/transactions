﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.CardsRepos
{
    public class CardsEFRepository : EFRepository<CARDS>, ICardsRepository
    {
        public CardsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override CARDS GetById(int id)
        {
            return Get(akt => akt.CARD_ID == id).SingleOrDefault();
        }

        public void Modify(int id, string cardnumber, DateTime expiration, string status, string type, bool additional, int owner)
        {
            CARDS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            if (cardnumber.Length == 16 && IsNumbers(cardnumber))
            {
                akt.CARD_NUMBER = cardnumber;
            }
            else
            {
                throw new ArgumentException("A kártya száma csak 16 számjegyet tartalmazhat.");
            }
            akt.EXPIRATION_DATE = expiration;
            if (status == "Aktív" || status == "Inaktív" || status == "Letiltott" || status == "Lejárt")
            {
                akt.STATUS = status;
            }
            else
            {
                throw new ArgumentException("A státusz nem megfelelő. (Aktív, Inaktív, Letiltott, Lejárt)");
            }

            if (type == "Forint" || type == "Hitel" || type == "Deviza-USD" || type == "Deviza-EUR")
            {
                akt.CARD_TYPE = type;
            }
            else
            {
                throw new ArgumentException("A típus nem megfelelő. (Forint, Hitel, Deviza-USD, Deviza-EUR)");
            }

            akt.ADDITIONAL_CARD = additional;

            akt.CARD_OWNER = owner;

            context.SaveChanges();
        }

        static bool IsNumbers(string input)
        {
            foreach (char c in input)
            {
                if (!Char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}