﻿using Data;
using Repository.GenericRepos;

namespace Repository.CardAccountsConnectionsRepos
{
    public interface ICardAccountsConnectionsRepository : IRepository<CARD_ACCOUNT_CONNECTIONS>
    {
        void Modify(int id, int cardid, int accountid);
    }
}