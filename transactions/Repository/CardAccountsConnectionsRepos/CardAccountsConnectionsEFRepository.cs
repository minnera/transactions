﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.CardAccountsConnectionsRepos
{
    public class CardAccountsConnectionsEFRepository : EFRepository<CARD_ACCOUNT_CONNECTIONS>, ICardAccountsConnectionsRepository
    {
        public CardAccountsConnectionsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override CARD_ACCOUNT_CONNECTIONS GetById(int id)
        {
            return Get(akt => akt.CARD_ACCOUNT_ID == id).SingleOrDefault();
        }

        public void Modify(int id, int cardid, int accountid)
        {
            CARD_ACCOUNT_CONNECTIONS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            akt.C_A_CARD_ID = cardid;
            akt.C_A_ACCOUNT_ID = accountid;

            context.SaveChanges();
        }
    }
}