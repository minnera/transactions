﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.AddressesRepos
{
    public class AddressesEFRepository : EFRepository<ADDRESSES>, IAddressesRepository
    {
        public AddressesEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override ADDRESSES GetById(int id)
        {
            return Get(akt => akt.ADDRESS_ID == id).SingleOrDefault();
        }

        public void Modify(int id, string address)
        {
            ADDRESSES akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            akt.ADDRESS = address;

            context.SaveChanges();
        }
    }
}