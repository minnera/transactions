﻿using Data;
using Repository.GenericRepos;

namespace Repository.AddressesRepos
{
    public interface IAddressesRepository : IRepository<ADDRESSES>
    {
        void Modify(int id, string address);
    }
}