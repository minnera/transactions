﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.TradersRepos
{
    public class TradersEFRepository : EFRepository<TRADERS>, ITradersRepository
    {
        public TradersEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override TRADERS GetById(int id)
        {
            return Get(akt => akt.TRADER_ID == id).SingleOrDefault();
        }

        public void Modify(int id, string name)
        {
            TRADERS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            if (name != null)
            {
                akt.TRADER_NAME = name;
            }

            context.SaveChanges();
        }
    }
}