﻿using Data;
using Repository.GenericRepos;

namespace Repository.TradersRepos
{
    public interface ITradersRepository : IRepository<TRADERS>
    {
        void Modify(int id, string name);
    }
}