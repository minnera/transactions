﻿using Data;
using Repository.GenericRepos;

namespace Repository.TraderPersonConnectionsRepos
{
    public interface ITraderPersonConnectionsRepository : IRepository<TRADER_PERSON_CONNECTIONS>
    {
        void Modify(int id, int traderid, int personid);
    }
}