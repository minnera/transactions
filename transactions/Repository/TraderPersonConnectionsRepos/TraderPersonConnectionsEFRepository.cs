﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.TraderPersonConnectionsRepos
{
    public class TraderPersonConnectionsEFRepository : EFRepository<TRADER_PERSON_CONNECTIONS>, ITraderPersonConnectionsRepository
    {
        public TraderPersonConnectionsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override TRADER_PERSON_CONNECTIONS GetById(int id)
        {
            return Get(akt => akt.TRADER_PERSON_ID == id).SingleOrDefault();
        }

        public void Modify(int id, int traderid, int personid)
        {
            TRADER_PERSON_CONNECTIONS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            akt.T_P_PERSON_ID = personid;

            akt.T_P_TRADER_ID = traderid;

            context.SaveChanges();
        }
    }
}