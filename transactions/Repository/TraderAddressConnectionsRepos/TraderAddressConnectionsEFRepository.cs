﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.TraderAddressConnectionsRepos
{
    public class TraderAddressConnectionsEFRepository : EFRepository<TRADER_ADDRESS_CONNECTIONS>, ITraderAddressConnectionsRepository
    {
        public TraderAddressConnectionsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override TRADER_ADDRESS_CONNECTIONS GetById(int id)
        {
            return Get(akt => akt.TRADER_ADDRESS_ID == id).SingleOrDefault();
        }

        public void Modify(int id, int traderid, int addressid)
        {
            TRADER_ADDRESS_CONNECTIONS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            akt.T_A_ADDRESS_ID = addressid;

            akt.T_A_TRADER_ID = traderid;

            context.SaveChanges();
        }
    }
}