﻿using Data;
using Repository.GenericRepos;

namespace Repository.TraderAddressConnectionsRepos
{
    public interface ITraderAddressConnectionsRepository : IRepository<TRADER_ADDRESS_CONNECTIONS>
    {
        void Modify(int id, int traderid, int addressid);
    }
}