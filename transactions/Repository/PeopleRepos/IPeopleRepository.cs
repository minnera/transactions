﻿using System;
using Data;
using Repository.GenericRepos;

namespace Repository.PeopleRepos
{
    public interface IPeopleRepository : IRepository<PEOPLE>
    {
        void Modify(int id, string lastname, string firstname, string email, string password,
            string phone);

        void ModifyPassword(int id, string password);
    }
}