﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Data;
using Liphsoft.Crypto.Argon2;
using Repository.GenericRepos;

namespace Repository.PeopleRepos
{
    public class PeopleEFRepository : EFRepository<PEOPLE>, IPeopleRepository
    {
        public PeopleEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override PEOPLE GetById(int id)
        {
            return Get(akt => akt.PERSON_ID == id).SingleOrDefault();
        }

        public void Modify(int id, string lastname, string firstname, string email, string password,
            string phone)
        {
            PEOPLE akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            if (lastname != null)
            {
                akt.LAST_NAME = lastname;
            }

            if (firstname != null)
            {
                akt.FIRST_NAME = firstname;
            }

            if (email != null)
            {
                akt.EMAIL = email;
            }


            if (password != null)
            {
                var hasher = new PasswordHasher();

                string salt = GetSalt(95);

                string myhash = hasher.Hash(password + salt);

                akt.PASSWORD = myhash;

                akt.SALT = salt;

                akt.PW_CHANGE_DATE = DateTime.Now;
            }

            if (phone != null)
            {
                akt.PHONE = phone;
            }

            context.SaveChanges();
        }


        //https://stackoverflow.com/a/1344255/3079988
        static string GetSalt(int size)
        {
            char[] chars = new char[62];
            chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[size];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public void ModifyPassword(int id, string password)
        {
            PEOPLE akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            var hasher = new PasswordHasher();

            string salt = GetSalt(95);

            string myhash = hasher.Hash(password + salt);

            akt.PASSWORD = myhash;

            akt.SALT = salt;

            akt.PW_CHANGE_DATE = DateTime.Now;

            context.SaveChanges();
        }
    }
}