﻿using System;
using Data;
using Repository.GenericRepos;

namespace Repository.TransactionsRepos
{
    public interface ITransactionsRepository : IRepository<TRANSACTIONS>
    {
        //date-ből kiszedhető day és time
        void Modify(int id, DateTime date, double amount, string type, string cardnumber, int traderid);
    }
}