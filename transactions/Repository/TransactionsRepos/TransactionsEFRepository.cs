﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.TransactionsRepos
{
    public class TransactionsEFRepository : EFRepository<TRANSACTIONS>, ITransactionsRepository
    {
        public TransactionsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override TRANSACTIONS GetById(int id)
        {
            return Get(akt => akt.TRANSACTION_ID == id).SingleOrDefault();
        }

        public void Modify(int id, DateTime date, double amount, string type, string cardnumber, int traderid)
        {
            TRANSACTIONS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }

            akt.DAY = date.Date;

            akt.TIME = date.TimeOfDay;

            akt.AMOUNT = amount;

            if (type == "Normál" || type == "Sztornó")
            {
                akt.TRANSACTION_TYPE = type;
            }
            else
            {
                throw new ArgumentException("A típus nem megfelelő. (Normál, Sztornó)");
            }

            if (cardnumber.Length == 16 && IsNumbers(cardnumber))
            {
                akt.CARD_NUMBER = cardnumber;
            }
            else
            {
                throw new ArgumentException("A kártya száma csak 16 számjegyet tartalmazhat.");
            }

            akt.TRADER = traderid;

            context.SaveChanges();
        }

        static bool IsNumbers(string input)
        {
            foreach (char c in input)
            {
                if (!Char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }
    }
}