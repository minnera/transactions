﻿using Data;
using Repository.GenericRepos;

namespace Repository.AccountsRepos
{
    public interface IAccountsRepository : IRepository<ACCOUNTS>
    {
        void Modify(int id, double balance, int owner, string type);
    }
}