﻿using System;
using System.Data.Entity;
using System.Linq;
using Data;
using Repository.GenericRepos;

namespace Repository.AccountsRepos
{
    public class AccountsEFRepository : EFRepository<ACCOUNTS>, IAccountsRepository
    {
        public AccountsEFRepository(DbContext newcontext) : base(newcontext)
        {
        }

        public override ACCOUNTS GetById(int id)
        {
            return Get(akt => akt.ACCOUNT_ID == id).SingleOrDefault();
        }
        public void Modify(int id, double balance, int owner, string type)
        {
            ACCOUNTS akt = GetById(id);
            if (akt == null)
            {
                throw new ArgumentException("A megadott id-val nem létezik elem.");
            }
            akt.BALANCE = balance;
            akt.ACCOUNT_OWNER = owner;
            if (type == "Betéti" || type == "Hitel" || type == "Deviza")
            {
                akt.ACCOUNT_TYPE = type;
            }
            else
            {
                throw new ArgumentException("A típus nem megfelelő. (Betéti, Hitel, Deviza)");
            }

            context.SaveChanges();
        }
    }
}