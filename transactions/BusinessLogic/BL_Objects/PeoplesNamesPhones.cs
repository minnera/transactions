﻿namespace BusinessLogic.BL_Objects
{
    public class PeoplesNamesPhones
    {
        public string LastName { set; get; }
        public string FirstName { set; get; }
        public string Phone { set; get; }
    }
}