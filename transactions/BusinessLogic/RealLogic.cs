﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BusinessLogic.BL_Objects;
using Data;
using Liphsoft.Crypto.Argon2;
using Repository;
using Repository.AccountsRepos;
using Repository.AddressesRepos;
using Repository.CardAccountsConnectionsRepos;
using Repository.CardsRepos;
using Repository.PeopleRepos;
using Repository.TraderAddressConnectionsRepos;
using Repository.TraderPersonConnectionsRepos;
using Repository.TradersRepos;
using Repository.TransactionsRepos;

namespace BusinessLogic
{
    public class RealLogic : ILogic
    {
        private MyRepository Repo;

        public RealLogic()
        {
            DatabaseEntities DE = new DatabaseEntities();
            AccountsEFRepository acr = new AccountsEFRepository(DE);
            AddressesEFRepository adr = new AddressesEFRepository(DE);
            CardAccountsConnectionsEFRepository cacr = new CardAccountsConnectionsEFRepository(DE);
            CardsEFRepository cr = new CardsEFRepository(DE);
            PeopleEFRepository pr = new PeopleEFRepository(DE);
            TraderAddressConnectionsEFRepository tacr = new TraderAddressConnectionsEFRepository(DE);
            TraderPersonConnectionsEFRepository tpcr = new TraderPersonConnectionsEFRepository(DE);
            TradersEFRepository tdr = new TradersEFRepository(DE);
            TransactionsEFRepository tcr = new TransactionsEFRepository(DE);

            Repo = new MyRepository(acr, adr, cacr, cr, pr, tacr, tpcr, tdr, tcr);
        }

        public RealLogic(MyRepository newRepo)
        {
            Repo = newRepo;
        }

        //---------------------------------------------------------------------------------------------

        public void AddAccount(double balance, int ownerId, string accountType)
        {
            //létezik e a tulajdonos?
            PEOPLE p = Repo.peopleRepo.GetById(ownerId);
            if (p == null)
            {
                throw new ArgumentException("Nem létezik a megadott felhasználó ID-n ember.");
            }
            if (accountType == null)
            {
                throw new ArgumentException("Nem lehet üres a folyószámla típusa.");
            }
            if (accountType != "Betéti" && accountType != "Hitel" && accountType != "Deviza")
            {
                throw new ArgumentException("A típus nem megfelelő. (Betéti, Hitel, Deviza)");
            }

            ACCOUNTS newAccount = new ACCOUNTS()
            {
                ACCOUNT_ID = GetNextAccountId(),
                ACCOUNT_OWNER = ownerId,
                ACCOUNT_TYPE = accountType,
                BALANCE = balance,
                PEOPLE = p
            };

            Repo.accountsRepo.Insert(newAccount);
        }

        public void AddAddress(string address)
        {
            if (address == null)
            {
                throw new ArgumentException("Nem lehet üres a cím szövege.");
            }

            ADDRESSES newAddress = new ADDRESSES()
            {
                ADDRESS_ID = GetNextAddressId(),
                ADDRESS = address
            };

            Repo.addressesRepo.Insert(newAddress);
        }

        public void AddCard(string cardNumber, DateTime expirationDate, string cardStatus, string cardType, int ownerId,
            int accountId)
        {
            if (cardNumber == null || cardStatus == null || cardType == null)
            {
                throw new ArgumentException("Nem lehet üres a kártyaszám, a kártyaállapot, sem a kártyatípus.");
            }
            if (cardNumber.Length != 16 || !IsNumbers(cardNumber))
            {
                throw new ArgumentException("A kártyaszámnak pontosan 16 db számjegynek kell lennie (0-9).");
            }
            if (cardType != "Forint" && cardType != "Hitel" && cardType != "Deviza-USD" && cardType != "Deviza-EUR")
            {
                throw new ArgumentException("A kártyatípus nem megfelelő. (Forint, Hitel, Deviza-USD vagy Deviza-EUR lehet)");
            }
            if (cardStatus != "Aktív" && cardStatus != "Inaktív" && cardStatus != "Letiltott" && cardStatus != "Lejárt")
            {
                throw new ArgumentException("A státusz nem megfelelő. (Aktív, Inaktív, Letiltott vagy Lejárt lehet)");
            }
            PEOPLE p = Repo.peopleRepo.GetById(ownerId);
            if (p == null)
            {
                throw new ArgumentException("Nem létezik a megadott felhasználó ID-n ember.");
            }
            ACCOUNTS a = Repo.accountsRepo.GetById(accountId);
            if (a == null)
            {
                throw new ArgumentException("Nem létezik a megadott folyószámla ID-n számla.");
            }
            int cardId = GetNextCardId();
            CARDS newCard = new CARDS()
            {
                ADDITIONAL_CARD = (ownerId != a.ACCOUNT_OWNER),
                CARD_ID = cardId,
                CARD_NUMBER = cardNumber,
                CARD_OWNER = ownerId,
                CARD_TYPE = cardType,
                STATUS = cardStatus,
                EXPIRATION_DATE = expirationDate,
                PEOPLE = p
            };

            Repo.cardsRepo.Insert(newCard);

            //hozzáadjuk a kártya-folyószámla kapcsolatot is

            CARD_ACCOUNT_CONNECTIONS newConnection = new CARD_ACCOUNT_CONNECTIONS()
            {
                ACCOUNTS = a,
                CARDS = Repo.cardsRepo.GetById(cardId),
                CARD_ACCOUNT_ID = GetNextCardAccountConnId(),
                C_A_CARD_ID = cardId,
                C_A_ACCOUNT_ID = a.ACCOUNT_ID
            };

            Repo.cardAccountsConnectionsRepo.Insert(newConnection);
        }

        static bool IsNumbers(string input)
        {
            foreach (char c in input)
            {
                if (!Char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        public void AddPerson(string lastName, string firstName, string email, string password, string phone)
        {
            if (lastName == null || firstName == null || email == null || password == null)
            {
                throw new ArgumentException("Nem lehet üres a vezetéknév, a keresztnév, az email, sem a jelszó.");
            }

            foreach (PEOPLE p in Repo.peopleRepo.GetAll())
            {
                if (p.EMAIL.Equals(email))
                {
                    throw new ArgumentException("A megadott emaillel már létezik felhasználó.");
                }
            }
            var hasher = new PasswordHasher();

            string salt = GetSalt(95);

            string myhash = hasher.Hash(password + salt);

            PEOPLE newPerson = new PEOPLE()
            {
                PERSON_ID = GetNextPersonId(),
                LAST_NAME = lastName,
                FIRST_NAME = firstName,
                EMAIL = email,
                PASSWORD = myhash,
                CREATE_DATE = DateTime.Now,
                PHONE = phone,
                SALT = salt
            };

            Repo.peopleRepo.Insert(newPerson);
        }

        //https://stackoverflow.com/a/1344255/3079988
        static string GetSalt(int size)
        {
            char[] chars = new char[62];
            chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[size];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }
            return result.ToString();
        }

        public void AddTrader(string name)
        {
            if (name == null)
            {
                throw new ArgumentException("Nem lehet üres a kereskedő neve.");
            }

            TRADERS newTrader = new TRADERS()
            {
                TRADER_ID = GetNextTraderId(),
                TRADER_NAME = name
            };

            Repo.tradersRepo.Insert(newTrader);
        }

        public void AddTransaction(DateTime date, double amount, string transactionType, string cardNumber, int traderId)
        {
            if (transactionType == null || cardNumber == null)
            {
                throw new ArgumentException("Nem lehet üres a típus, sem a kártyaszám.");
            }
            if (transactionType != "Normál" && transactionType != "Sztornó")
            {
                throw new ArgumentException("Nem megfelelő a tranzakció típusa. (Normál vagy Sztornó lehet)");
            }
            if (cardNumber.Length != 16 || !IsNumbers(cardNumber))
            {
                throw new ArgumentException("A kártyaszámnak pontosan 16 db számjegynek kell lennie (0-9).");
            }

            TRADERS t = Repo.tradersRepo.GetById(traderId);
            if (t == null)
            {
                throw new ArgumentException("Nem létezik a megadott kereskedő ID-n kereskedő.");
            }

            CARDS thisCard = null;
            foreach (CARDS c in Repo.cardsRepo.GetAll())
            {
                if (c.CARD_NUMBER.Equals(cardNumber))
                {
                    thisCard = c;
                }
            }

            if (thisCard == null)
            {
                throw new ArgumentException("Nem létezik a megadott kártyaszámon kártya.");
            }

            TRANSACTIONS newTransaction = new TRANSACTIONS()
            {
                TRANSACTION_ID = GetNextTransactionId(),
                DAY = date.Date,
                TIME = date.TimeOfDay,
                AMOUNT = amount,
                TRANSACTION_TYPE = transactionType,
                CARD_NUMBER = cardNumber,
                TRADER = traderId,
                TRADERS = t
            };

            Repo.transactionsRepo.Insert(newTransaction);
        }

        public void AddAddressToTrader(string address, int traderId)
        {
            //nem nézzük, van e cím ismétlődés a külalak változatossága miatt
            if (address == null)
            {
                throw new ArgumentException("Nem lehet üres a cím szövege.");
            }
            int addressId = GetNextAddressId();
            ADDRESSES newAddress = new ADDRESSES()
            {
                ADDRESS_ID = addressId,
                ADDRESS = address
            };

            Repo.addressesRepo.Insert(newAddress);

            //kapcsolat hozzáadása

            //létezik a kereskedő?

            TRADERS t = Repo.tradersRepo.GetById(traderId);
            if (t == null)
            {
                throw new ArgumentException("Nem létezik kereskedő a megadott ID-n.");
            }

            TRADER_ADDRESS_CONNECTIONS newConnection = new TRADER_ADDRESS_CONNECTIONS()
            {
                TRADER_ADDRESS_ID = GetNextTraderAddressConnId(),
                ADDRESSES = Repo.addressesRepo.GetById(addressId),
                T_A_ADDRESS_ID = addressId,
                T_A_TRADER_ID = traderId,
                TRADERS = t
            };

            Repo.traderAddressConnectionsRepo.Insert(newConnection);
        }

        public void AddContactToTrader(int traderId, int personId)
        {
            //létezik a kereskedő?

            TRADERS t = Repo.tradersRepo.GetById(traderId);
            if (t == null)
            {
                throw new ArgumentException("Nem létezik kereskedő a megadott ID-n.");
            }

            //létezik a felhasználó?

            PEOPLE p = Repo.peopleRepo.GetById(personId);
            if (p == null)
            {
                throw new ArgumentException("Nem létezik felhasználó a megadott ID-n.");
            }

            TRADER_PERSON_CONNECTIONS newConnection = new TRADER_PERSON_CONNECTIONS()
            {
                PEOPLE = p,
                TRADERS = t,
                TRADER_PERSON_ID = GetNextTraderPersonConnId(),
                T_P_PERSON_ID = personId,
                T_P_TRADER_ID = traderId
            };

            Repo.traderPersonConnectionsRepo.Insert(newConnection);
        }

        public IQueryable<ACCOUNTS> GetAccounts()
        {
            return Repo.accountsRepo.GetAll();
        }

        public IQueryable<ADDRESSES> GetAddresses()
        {
            return Repo.addressesRepo.GetAll();
        }

        public IQueryable<CARDS> GetCards()
        {
            return Repo.cardsRepo.GetAll();
        }

        public IQueryable<PEOPLE> GetPeople()
        {
            return Repo.peopleRepo.GetAll();
        }

        public IQueryable<TRADERS> GetTraders()
        {
            return Repo.tradersRepo.GetAll();
        }

        public IQueryable<TRANSACTIONS> GetTransactions()
        {
            return Repo.transactionsRepo.GetAll();
        }

        public IQueryable<PeoplesNamesPhones> GetNamesPhones()
        {
            var q1 = from people in Repo.peopleRepo.GetAll()
                select new PeoplesNamesPhones()
                {
                    LastName = people.LAST_NAME,
                    FirstName = people.FIRST_NAME,
                    Phone = people.PHONE
                };
            return q1;
        }

        public IQueryable<CARDS> GetPersonsCards(int id)
        {
            var q1 = from cards in Repo.cardsRepo.GetAll()
                where cards.CARD_OWNER == id
                select cards;
            return q1;
        }

        public IQueryable<TRANSACTIONS> GetPersonsTransactions(int id)
        {
            var q1 = from transactions in Repo.transactionsRepo.GetAll()
                join cardcons in Repo.cardsRepo.GetAll() on transactions.CARD_NUMBER equals cardcons.CARD_NUMBER
                where cardcons.CARD_OWNER == id
                select transactions;
            return q1;
        }

        public IQueryable<TRANSACTIONS> GetCardsTransactions(int id)
        {
            var q1 = from transactions in Repo.transactionsRepo.GetAll()
                     join cardcons in Repo.cardsRepo.GetAll() on transactions.CARD_NUMBER equals cardcons.CARD_NUMBER
                where cardcons.CARD_ID == id
                select transactions;
            return q1;
        }

        public IQueryable<ACCOUNTS> GetPersonsAccounts(int id)
        {
            var q1 = from accounts in Repo.accountsRepo.GetAll()
                where accounts.ACCOUNT_OWNER == id
                select accounts;
            return q1;
        }

        public IQueryable<ADDRESSES> GetTradersAddresses(int id)
        {
            var q1 = from addresses in Repo.addressesRepo.GetAll()
                join cons in Repo.traderAddressConnectionsRepo.GetAll() on addresses.ADDRESS_ID equals cons
                    .T_A_ADDRESS_ID
                where cons.T_A_TRADER_ID == id
                select addresses;
            return q1;
        }

        public IQueryable<PeoplesNamesPhones> GetTradersContacts(int id)
        {
            var q1 = from people in Repo.peopleRepo.GetAll()
                join cons in Repo.traderPersonConnectionsRepo.GetAll() on people.PERSON_ID equals cons.T_P_PERSON_ID
                where cons.T_P_TRADER_ID == id
                select new PeoplesNamesPhones()
                {
                    LastName = people.LAST_NAME,
                    FirstName = people.FIRST_NAME,
                    Phone = people.PHONE
                };
            return q1;
        }

        public PEOPLE GetOnePerson(int id)
        {
            return Repo.peopleRepo.GetById(id);
        }

        public CARDS GetOneCard(int id)
        {
            return Repo.cardsRepo.GetById(id);
        }

        public ACCOUNTS GetOneAccount(int id)
        {
            return Repo.accountsRepo.GetById(id);
        }

        public TRANSACTIONS GetOneTransaction(int id)
        {
            return Repo.transactionsRepo.GetById(id);
        }

        public TRADERS GetOneTrader(int id)
        {
            return Repo.tradersRepo.GetById(id);
        }

        public ADDRESSES GetOneAddress(int id)
        {
            return Repo.addressesRepo.GetById(id);
        }

        public void DelPerson(int id)
        {
            Repo.peopleRepo.Delete(id);
        }

        public void DelAccount(int id)
        {
            Repo.accountsRepo.Delete(id);
        }

        public void DelTransaction(int id)
        {
            Repo.transactionsRepo.Delete(id);
        }

        public void DelCard(int id)
        {
            Repo.cardsRepo.Delete(id);
        }

        public void DelTrader(int id)
        {
            Repo.tradersRepo.Delete(id);
        }

        public void DelAddress(int id)
        {
            Repo.addressesRepo.Delete(id);
        }

        public void ModifyAccount(int id, double balance, int ownerId, string accountType)
        {
            if (Repo.peopleRepo.GetById(ownerId) == null)
            {
                throw new ArgumentException("Nem létezik felhasználó a megadott ID-n.");
            }

            Repo.accountsRepo.Modify(id, balance, ownerId, accountType);
        }

        public void ModifyAddress(int id, string address)
        {
            Repo.addressesRepo.Modify(id, address);
        }

        public void ModifyCard(int id, string cardNumber, DateTime expirationDate, string cardStatus, string cardType, int ownerId)
        {
            if (Repo.cardsRepo.GetById(id) == null)
            {
                throw new ArgumentException("Nem létezik kártya a megadott ID-n.");
            }

            if (Repo.peopleRepo.GetById(ownerId) == null)
            {
                throw new ArgumentException("Nem létezik felhasználó a megadott ID-n.");
            }

            bool additionalcard = false;
            foreach (CARD_ACCOUNT_CONNECTIONS cac in Repo.cardAccountsConnectionsRepo.GetAll())
            {
                if (cac.C_A_CARD_ID == id && Repo.accountsRepo.GetById(cac.CARD_ACCOUNT_ID).ACCOUNT_OWNER != ownerId)
                {
                    additionalcard = true;
                }
            }

            Repo.cardsRepo.Modify(id, cardNumber, expirationDate, cardStatus, cardType, additionalcard, ownerId);
        }

        public void ModifyPerson(int id, string lastName, string firstName, string email, string password, string phone)
        {
            foreach (PEOPLE p in Repo.peopleRepo.GetAll())
            {
                if (p.EMAIL.Equals(email))
                {
                    throw new ArgumentException("A megadott emaillel már létezik felhasználó.");
                }
            }
            Repo.peopleRepo.Modify(id, lastName, firstName, email, password, phone);
        }

        public void ModifyPersonsPassword(int id, string password)
        {
            Repo.peopleRepo.ModifyPassword(id, password);
        }

        public void ModifyTransaction(int id, DateTime date, double amount, string transactionType, string cardNumber, int traderId)
        {
            if (Repo.tradersRepo.GetById(traderId) == null)
            {
                throw new ArgumentException("Nem létezik kereskedő a megadott ID-n.");
            }
            Repo.transactionsRepo.Modify(id, date, amount, transactionType, cardNumber, traderId);
        }

        public bool Login(string email, string password)
        {
            bool output = false;
            var hasher = new PasswordHasher();

            foreach (PEOPLE p in Repo.peopleRepo.GetAll())
            {
                if (hasher.Verify(p.PASSWORD, (password + p.SALT)))
                {
                    // user entered correct password
                    output = true;
                }
            }

            return output;
        }

        public int GetNextAccountId()
        {
            if (!Repo.accountsRepo.GetAll().Any()) return 1;
            return Repo.accountsRepo.GetAll().Max(x => (int)x.ACCOUNT_ID) + 1;
        }

        public int GetNextAddressId()
        {
            if (!Repo.addressesRepo.GetAll().Any()) return 1;
            return Repo.addressesRepo.GetAll().Max(x => (int)x.ADDRESS_ID) + 1;
        }

        public int GetNextCardId()
        {
            if (!Repo.cardsRepo.GetAll().Any()) return 1;
            return Repo.cardsRepo.GetAll().Max(x => (int)x.CARD_ID) + 1;
        }

        public int GetNextPersonId()
        {
            if (!Repo.peopleRepo.GetAll().Any()) return 1;
            return Repo.peopleRepo.GetAll().Max(x => (int)x.PERSON_ID) + 1;
        }

        public int GetNextTraderId()
        {
            if (!Repo.tradersRepo.GetAll().Any()) return 1;
            return Repo.tradersRepo.GetAll().Max(x => (int)x.TRADER_ID) + 1;
        }

        public int GetNextTransactionId()
        {
            if (!Repo.transactionsRepo.GetAll().Any()) return 1;
            return Repo.transactionsRepo.GetAll().Max(x => (int)x.TRANSACTION_ID) + 1;
        }

        public int GetNextCardAccountConnId()
        {
            if (!Repo.cardAccountsConnectionsRepo.GetAll().Any()) return 1;
            return Repo.cardAccountsConnectionsRepo.GetAll().Max(x => (int)x.CARD_ACCOUNT_ID) + 1;
        }

        public int GetNextTraderAddressConnId()
        {
            if (!Repo.traderAddressConnectionsRepo.GetAll().Any()) return 1;
            return Repo.traderAddressConnectionsRepo.GetAll().Max(x => (int)x.TRADER_ADDRESS_ID) + 1;
        }

        public int GetNextTraderPersonConnId()
        {
            if (!Repo.traderPersonConnectionsRepo.GetAll().Any()) return 1;
            return Repo.traderPersonConnectionsRepo.GetAll().Max(x => (int)x.TRADER_PERSON_ID) + 1;
        }
    }
}