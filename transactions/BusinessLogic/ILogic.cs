﻿using System;
using System.Linq;
using BusinessLogic.BL_Objects;
using Data;

namespace BusinessLogic
{
    public interface ILogic
    {
        //HOZZÁADÁS-------------------------------------------
        //folyószámla hozzáadása
        /// <summary>
        /// Megadott adatok alapján hozzáad az adatbázishoz egy új folyószámlát.
        /// </summary>
        /// <param name="balance">Kezdőegyenleg.</param>
        /// <param name="ownerId">A folyószámla tulajdonosának ID-ja.</param>
        /// <param name="accountType">Lehet: Betéti, Hitel, Deviza</param>
        void AddAccount(double balance, int ownerId, string accountType);

        //cím hozzáadása
        void AddAddress(string address);

        //kártya hozzáadása
        /// <summary>
        /// Megadott adatok alapján hozzáad az adatbázishoz egy új kártyát.
        /// </summary>
        /// <param name="cardNumber">16 db számjegyet tartalmazhat.</param>
        /// <param name="expirationDate">Lejárati dátum.</param>
        /// <param name="cardStatus">Kártya állapot, lehet: Aktív, Inaktív, Letiltott, Lejárt.</param>
        /// <param name="cardType">Kártya típus, lehet: Forint, Hitel, Deviza-USD, Deviza-EUR.</param>
        /// <param name="ownerId">A kártyatulajdonos ID-ja</param>
        /// <param name="accountId">A számlatulajdonos ID-ja (ha ez más, mint a kártyatulajdonos ID-ja, akkor társkártya)</param>
        void AddCard(string cardNumber, DateTime expirationDate, string cardStatus, string cardType, int ownerId, int accountId);

        //felhasználó hozzáadása
        void AddPerson(string lastName, string firstName, string email, string password, string phone);

        //kereskedő hozzáadása
        void AddTrader(string name);

        //tranzakció hozzáadása
        /// <summary>
        /// Megadott adatok alapján hozzáad az adatbázishoz egy új tranzakciót.
        /// </summary>
        /// <param name="date">Tranzakció ideje.</param>
        /// <param name="amount">Összege.</param>
        /// <param name="transactionType">Lehet: Normál, Sztornó</param>
        /// <param name="cardNumber">16 számjegynek és már létező kártya számának kell lennie.</param>
        /// <param name="traderId">Kereskedő ID-ja.</param>
        void AddTransaction(DateTime date, double amount, string transactionType, string cardNumber, int traderId);

        //---------------------------------------------------------------

        //új cím hozzáadása kereskedőhöz
        void AddAddressToTrader(string address, int traderId);

        //új kapcsolat hozzáadása kereskedőhöz (ezt egy felhasználó listából lehessen választani)
        void AddContactToTrader(int traderId, int personId);


        //ÖSSZES ELEM VISSZAADÁSA-----------------------------
        //összes folyószámla visszaadása (admin)
        IQueryable<ACCOUNTS> GetAccounts();

        //összes cím visszaadása
        IQueryable<ADDRESSES> GetAddresses();

        //összes kártya visszaadása (admin)
        IQueryable<CARDS> GetCards();

        //összes felhasználó visszaadása (admin)
        IQueryable<PEOPLE> GetPeople();

        //összes kereskedő visszaadása
        IQueryable<TRADERS> GetTraders();

        //összes tranzakció visszaadása (admin)
        IQueryable<TRANSACTIONS> GetTransactions();

            //EGYÉB OBJEKTUMOKAT VISSZAADÓ LEKÉRDEZÉSEK-----------
        //felhasználókat névvel, telefonnal visszaadó lekérdezés
        IQueryable<PeoplesNamesPhones> GetNamesPhones();

        //SZŰRT LISTÁK----------------------------------------
        //egy felhasználó kártyáit visszaadó művelet
        IQueryable<CARDS> GetPersonsCards(int id);

        //egy felhasználó összes tranzakciója
        IQueryable<TRANSACTIONS> GetPersonsTransactions(int id);

        //egy kártya tranzakciói
        IQueryable<TRANSACTIONS> GetCardsTransactions(int id);

        //egy felhasználó folyószámlái
        IQueryable<ACCOUNTS> GetPersonsAccounts(int id);

        //egy kereskedő címei
        IQueryable<ADDRESSES> GetTradersAddresses(int id);

        //egy kereskedő kapcsolatai
        IQueryable<PeoplesNamesPhones> GetTradersContacts(int id);

        //EGY ELEM VISSZAADÁSA---------------------------------
        //egy felhasználót visszaad
        PEOPLE GetOnePerson(int id);

        //egy kártyát visszaad
        CARDS GetOneCard(int id);

        //egy folyószámlát visszaad
        ACCOUNTS GetOneAccount(int id);

        //egy tranzakciót visszaad
        TRANSACTIONS GetOneTransaction(int id);

        //egy kereskedőt visszaad
        TRADERS GetOneTrader(int id);

        //egy címet visszaad
        ADDRESSES GetOneAddress(int id);

        //ELEM TÖRLÉSE------------------------------------------
        //felhasználó törlése
        void DelPerson(int id);

        //folyószámla törlése
        void DelAccount(int id);

        //tranzakció törlése
        void DelTransaction(int id);

        //kártya törlése
        void DelCard(int id);

        //kereskedő törlése
        void DelTrader(int id);

        //cím törlése
        void DelAddress(int id);

        //MÓDOSÍTÁS----------------------------------------------
        //folyószámla módosítása
        /// <summary>
        /// Megadott adatokkal módosítja az adatbázisban a folyószámlát.
        /// </summary>
        /// <param name="id">Módosítani kívánt folyószámla ID-ja.</param>
        /// <param name="balance">Új egyenleg.</param>
        /// <param name="ownerId">Új folyószámla tulajdonos ID-ja.</param>
        /// <param name="accountType">Új típusa, lehet: Betéti, Hitel, Deviza.</param>
        void ModifyAccount(int id, double balance, int ownerId, string accountType);

        //cím módosítása
        void ModifyAddress(int id, string address);

        //kártya módosítása
        /// <summary>
        /// Megadott adatokkal módosítja az adatbázisban a kártyát.
        /// </summary>
        /// <param name="id">Módosítani kívánt kártya ID-ja.</param>
        /// <param name="cardNumber">Új kártyaszám, 16 db számjegynek kell lennie.</param>
        /// <param name="expirationDate">Új lejárati dátum.</param>
        /// <param name="cardStatus">Új kártya állapot, lehet: Aktív, Inaktív, Letiltott, Lejárt.</param>
        /// <param name="cardType">Új kártya típus, lehet: Forint, Hitel, Deviza-USD, Deviza-EUR.</param>
        /// <param name="ownerId">A kártya új tulajdonosának ID-ja. Ha eltér a kártyához tartozó folyószámla tulajdonosától, akkor a kártya társkártya lesz.</param>
        void ModifyCard(int id, string cardNumber, DateTime expirationDate, string cardStatus, string cardType, int ownerId);

        //felhasználó módosítása
        void ModifyPerson(int id, string lastName, string firstName, string email, string password, string phone);

        //felhasználó jelszavának módosítása
        void ModifyPersonsPassword(int id, string password);

        //kereskedő módosítása

        //tranzakció módosítása
        /// <summary>
        /// Megadott adatokkal módosítja az adatbázisban a tranzakciót.
        /// </summary>
        /// <param name="id">Módosítani kívánt tranzakció ID-ja.</param>
        /// <param name="date">Tranzakció új ideje.</param>
        /// <param name="amount">Tranzakció új összege.</param>
        /// <param name="transactionType">Tranzakció új típusa, lehet: Normál, Sztornó.</param>
        /// <param name="cardNumber">Új kártyaszám, 16 db számjegynek és már létező kártyaszámnak kell lennie.</param>
        /// <param name="traderId">Új kereskedő ID-ja.</param>
        void ModifyTransaction(int id, DateTime date, double amount, string transactionType, string cardNumber, int traderId);

        //BELÉPÉS-------------------------------------------------
        //felhasználó belépés
        bool Login(string email, string password);

        //KÖVETKEZŐ ID GENERÁLÁSA---------------------------------
        //következő folyószámla ID
        int GetNextAccountId();

        //következő cím ID
        int GetNextAddressId();

        //következő kártya ID
        int GetNextCardId();

        //következő felhasználó ID
        int GetNextPersonId();

        //következő kereskedő ID
        int GetNextTraderId();

        //következő tranzakció ID
        int GetNextTransactionId();

        //következő kártya-folyószámla kapcsolat ID
        int GetNextCardAccountConnId();

        //következő kereskedő-cím kapcsolat ID
        int GetNextTraderAddressConnId();

        //következő kereskedő-felhasználó kapcsolat ID
        int GetNextTraderPersonConnId();

    }
}