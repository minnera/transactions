﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication.Models
{
    public class CardModel
    {
        [Display(Name = "Kártya ID")]
        [Required]
        public int CardId { get; set; }
        [Display(Name = "Kártya Szám")]
        [Required]
        public string CardNumber { get; set; }
        [Display(Name = "Lejárat")]
        [Required]
        public DateTime ExpirationDate { get; set; }
        [Display(Name = "Állapot")]
        [Required]
        public string Status { get; set; }
        [Display(Name = "Típus")]
        [Required]
        public string CardType { get; set; }
        [Display(Name = "Társkártya")]
        [Required]
        public bool AdditionalCard { get; set; }
        [Display(Name = "Tulajdonos Id")]
        [Required]
        public int CardOwner { get; set; }
    }
}